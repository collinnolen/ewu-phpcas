<?php
/**
* Authors: Danny Messina, James Reisenhauer, Andy Johnson, Collin Nolen, Jeremy Gay, Turner Lehmbecker
* 
* 
*/
namespace EWU\SingleSignOn;

class SSOClient
{
    private $client;
    private $username;
    private $attributes;

    /**
	* Constructor for SSO client
	*/
    public function __construct($protocol = 'SAML_VERSION_1_1')
    {
		
        	$this->username = null;
        	$this->attributes = array();
		
	
		
		//check initialization of CAS
		$initialized = \phpCAS::isInitialized();
		
		//if CAS isn't initialized, initialize it
		if(!$initialized)
		{
			require_once 'config.php';
            		$this->cas_host = $cas_host;
            		$this->cas_port = $cas_port;
            		$this->cas_context = $cas_context;
            		$this->cas_server_ca_cert_path = $cas_server_ca_cert_path;
            		$this->cas_debug_log = $cas_debug_log;
            		$this->cas_real_hosts = $cas_real_hosts;
			
			//set which protocol CAS should use
			switch($protocol)
			{
				case 'CAS_VERSION_1_0':
					\phpCAS::client(CAS_VERSION_1_0, $this->cas_host, $this->cas_port, $this->cas_context, true);
                    			break;
				case 'CAS_VERSION_2_0':
					\phpCAS::client(CAS_VERSION_2_0, $this->cas_host, $this->cas_port, $this->cas_context, true);
					break;
				case 'SAML_VERSION_1_1': //add support for SAML 2.0 later
				default:
					\phpCAS::client(SAML_VERSION_1_1, $this->cas_host, $this->cas_port, $this->cas_context);
			}
			
			//set the authentication certificate
			\phpCAS::setCasServerCACert($this->cas_server_ca_cert_path);
		}
		else //initialized, just need to check to see if CAS servers have already been set
		{
			if(!isset($this->cas_real_hosts))
				$this->cas_real_hosts = false;
		}
		
    }

	/**
	* Do the authentication
	* don't need to return the credentials after authentication
	* up to the app when credentials should be gathered
	*/
    public function authenticate()
    {
       
		
	//perform authentication
	\phpCAS::handleLogoutRequests(true, $this->cas_real_hosts);
	\phpCAS::forceAuthentication();
		
	$this->username = \phpCAS::getUser();
		
	//are there attributes associated with this user?
	if (\phpCAS::hasAttributes()) 
            $this->attributes = \phpCAS::getAttributes();
		
	//are we debugging?
	if (defined('EWU_SSO_DEBUG')) 
	{
            \phpCAS::setDebug($this->cas_debug_log);
            \phpCAS::setVerbose(true);
        }

        return $this->credentials();


    }

    public function logout()
    {
	\phpCAS::logout();
    }
	
	
    public function get_user()
    {
	return $this->username;
    }

    public function get_attributes()
    {
	return $this->attributes;
    }
	
    private function credentials(){
        $result = array();

        $result['username'] = $this->get_user();
        $result['attributes'] = $this->get_attributes();

        return $result;
    }
}
