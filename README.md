# EWU Single Sign On Library

EWU PHP library providing a preconfigured standard for implementing EWU SSO. Supported authentication protocols: 

  - SAML 1.1
  - CAS 2.0
  - CAS 1.0

# Installation
Installation is done using [Git](https://git-scm.com/) & [Composer](https://getcomposer.org/)

## Requirements
In order to proceed with installation you will need Git installed and properly configured in your environment (see the above link for instructions on how to do so). The reason for this is that Composer will be using Git to install source files. Additionally you will need PHP 5.3.2 or greater to use Composer. Lastly, you will need the latest version of the Composer dependency manager installed. See the above link for how to do so for your environment.

Additionally, this library is available for use only on a trusted EWU network and is hosted in the Project Builder Git repository. 

## Proceeding with installation
After the above requirements are met, simply open up the command line in the directory you wish to install the package and type the following command: 

```
composer require ewu/sso
```

The library and it's dependencies will be downloaded to the directory where the command was issued.
 
# Initialization
 
Once you have installed the necessary files using the steps outlined above, you will need to do the following in order to use the EWU SSO library within you application:

1. Add `require './vendor/autoload.php';` to your application's main source file (before any other actual code).
2. Instantiate/initialize an SSO client like so: `$sso = new EWU\SingleSignOn\SSOClient();`
3. Execute the client's `authenticate()` method and retrieve credentials/attributes, i.e. `$results = $sso->authenticate();`

**NOTE: If you cloned the repo into a directory other than your web directory, you will need to change the** `require './vendor/autoload.php';` **in step 1 to something like** `require '~/path/to/SSO_client/vendor/autoload.php';`.

Here is an example:

```php
require './vendor/autoload.php';

$sso = new EWU\SingleSignOn\SSOClient();
$results = $sso->authenticate();

# Gets the user's netID
$username = $sso->get_user();
echo "Hello , ".$username.", you are authenticated!";
```

To retrieve additional attributes about the user use the following:

```php
# Get any additional CAS attributes 
$attributes = $sso->get_attributes();
#Alternatively you could do this assuming you retrieved attributes into $results during authentication
#$attributes = $results['attributes'];
ksort($attributes);
foreach ($attributes as $key => $value) {
    if (is_array($value)) {
        echo '<li><strong>', $key, ':<ul>';
        foreach ($value as $item) {
            echo '<li></strong>', $item, '</li>';
        }
        echo '</ul></li>';
    } else {
        echo '<li><strong>', $key, ': </strong>', $value, '</li>'.PHP_EOL;
    }
}
```

This will print out a list of attribute names and their associated values for the authenticated user. From there, you can keep note of attribute names to make accessing them easier using an associative array. For example:

```php
$attributes = $results['attributes'];
$some_user_attribute = $attributes['some_user_attribute_name'];
```
 
# Protocols
 By default, this library uses the SAML 1.1 protocol which enables SSO attributes for authorized services. To change the protocol being used, provide a different protocol into the SSO client's contructor.
 - SAML 1.1 = `'SAML_VERSION_1_1'`
 - CAS 2.0 = `'CAS_VERSION_2_0'`
 - CAS 1.0 = `'CAS_VERSion_1_0'`
 
 For example:
```php
require './vendor/autoload.php';

$samlClient = new EWU\SingleSignOn\SSOClient('SAML_VERSION_1_1');
$cas2Client = new EWU\SingleSignOn\SSOClient('CAS_VERSION_2_0');
$cas1Client = new EWU\SingleSignOn\SSOClient('CAS_VERSION_1_0');
```

## Version
1.0.0

